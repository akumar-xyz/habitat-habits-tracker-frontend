import { Component, OnInit } from '@angular/core';
import {FormControl, Validators} from '@angular/forms';
import { Router } from '@angular/router';
import { RestService } from '../rest.service';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import {MatSnackBar} from '@angular/material';

@Component({
	selector: 'app-add-habit',
	templateUrl: './add-habit.component.html',
	styleUrls: ['./add-habit.component.scss']
})
export class AddHabitComponent implements OnInit {

	hQuestion = new FormControl('', [
		Validators.required,
	]);

	hName = new FormControl('', [
		Validators.required
	]);

	isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
		.pipe(
			map(result => result.matches)
		);

	constructor(private breakpointObserver: BreakpointObserver, public rest:RestService, private router: Router, public snackBar: MatSnackBar) {}

	addHabit() {
		var name = this.hName.value;
		var question = this.hQuestion.value;
		var bod = {
			"habit_name" : name,
			"question" : question
		}
		this.rest.addHabit(bod).subscribe( (resp ) => {

			if ( resp.message === "success" ) {
				this.snackBar.open('Successfully added your habit. We will track it from tomorrow', 'Okay').afterDismissed().subscribe(() =>{
					this.router.navigate(['/dashboard']);
				});
			}
			else{
				console.log(resp);
			}

		})
	}

	ngOnInit() {
	}

}
