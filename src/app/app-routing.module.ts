import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AboutComponent} from './about/about.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { GameComponent } from './game/game.component';
import { AuthGuard } from './auth.guard';
import { AddHabitComponent } from './add-habit/add-habit.component';
import { ManageHabitsComponent } from './manage-habits/manage-habits.component';
import { HabitDetailsComponent } from './habit-details/habit-details.component';

const routes: Routes = [
	{
		path : '',
		redirectTo : 'about',
		pathMatch : 'full'
	},
	{
		path : 'about',
		component : AboutComponent
	},
	{
		path : 'login',
		component : LoginComponent
	},
	{
		path : 'register',
		component : RegisterComponent
	},
	{
		path : 'dashboard',
		component : DashboardComponent,
		canActivate : [AuthGuard]
	},
	{
		path : 'game',
		component : GameComponent
	},
	{
		path : 'addHabit',
		component : AddHabitComponent,
		canActivate : [AuthGuard],
	},
	{
		path : 'manage',
		component : ManageHabitsComponent,
		canActivate : [AuthGuard],
	},
	{
		path : 'details',
		component : ManageHabitsComponent,
		canActivate : [AuthGuard],
	},


];

@NgModule({
	imports: [RouterModule.forRoot(routes)],
	exports: [RouterModule]
})
export class AppRoutingModule { }
