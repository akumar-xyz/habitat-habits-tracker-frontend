import { Component, OnInit } from '@angular/core';
import {FormControl, Validators} from '@angular/forms';
import { Router } from '@angular/router';
import { RestService } from '../rest.service';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import {MatSnackBar} from '@angular/material';
import { Input } from '@angular/core';

@Component({
	selector: 'app-habit-details',
	templateUrl: './habit-details.component.html',
	styleUrls: ['./habit-details.component.scss']
})
export class HabitDetailsComponent implements OnInit {

	@Input () id : string;
	strength : number;

	isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
		.pipe(
			map(result => result.matches)
		);

	constructor(private breakpointObserver: BreakpointObserver, public rest:RestService, private router: Router, public snackBar: MatSnackBar) {}

	ngOnInit() {
		this.rest.habitData( this.id ).subscribe((resp) =>{
			let dates = resp['today'].map( resp => resp.today );
			let vals = resp['list'].map( resp => resp.checked );

			for(var i = 0; i < vals.length; ++i){
				var count;
				if(vals[i] == 0)
					count++;
			}
			this.strength = count;

			console.log( dates, vals, this.strength );


		})
	}

}
