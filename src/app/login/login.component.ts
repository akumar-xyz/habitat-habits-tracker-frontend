import { Component, OnInit } from '@angular/core';
import {FormControl, Validators} from '@angular/forms';
import { Router } from '@angular/router';
import { RestService } from '../rest.service';
import {MatSnackBar} from '@angular/material';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  emailFormControl = new FormControl('', [
    Validators.required,
    Validators.email,
  ]);

  passwordFormControl = new FormControl('', [
    Validators.required
  ]);

  constructor(public rest:RestService, private router: Router, public snackBar: MatSnackBar) { }

  login() {
  	var email = this.emailFormControl.value;
	var passw = this.passwordFormControl.value;
	var bod = {
		"email" : email,
		"password" : passw
	};

	this.rest.login( bod ).subscribe((resp) => {
		localStorage.setItem("token", resp.token);
		localStorage.setItem("username", resp.user.name);
		this.router.navigate(['/dashboard']);
	});

  }

  ngOnInit() {
  }

}
