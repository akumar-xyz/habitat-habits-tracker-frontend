import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable, of } from 'rxjs';


@Injectable()
export class TokenInterceptorService implements HttpInterceptor {
 
  constructor() {}
 
  intercept(req: HttpRequest<any>, next: HttpHandler) {
	  
    // Get the auth token from the service.
    const authToken = localStorage.token;
 
    // Clone the request and replace the original headers with
    // cloned headers, updated with the authorization.
    const authReq = req.clone({
      headers: req.headers.set('Authorization', 'Bearer ' + authToken)
    });
 
    // send cloned request with header to the next handler.
    return next.handle(authReq);
  }
}
